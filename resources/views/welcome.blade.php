<!DOCTYPE html>
<html>
<head>
<style>
.center-block {
  display: block;
  margin-right: auto;
  margin-left: auto;
}

.myDiv {
  border: 5px outset red;
  background-color: lightblue;    
  text-align: center;
}
</style>

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
<a href="list">Go to Inbox</a> 
<a href="logout">Logout</a> 



<form method="POST" action="upload">
            @csrf
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Send Email</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <!-- Modal Body -->
            <div class="modal-body">
                    <!--<div class="mb-3">
                        <label for="subject" class="form-label" required>Subject</label>
                        <input type="text"  id="subject" name="subject">   
                    </div>
        
                    <div class="mb-3">
                        <label for="body" class="form-label" required>Email Body</label><br><br>
                        <textarea id="body" name="body" rows="4" cols="50"></textarea>
                    </div> -->
                    <input type="hidden"  id="hSubject" name="hSubject" value="">  
                    <input type="hidden"  id="hBody" name="hBody" value="">  

                    <div class="mb-3">
                        <label for="email" class="form-label" required>Emails</label><br><br>
                        <input type="text"  id="email" name="email">  
                    </div>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Send Email</button>
            </div>
            </div>
        </div>
        </div>
</form>

<div class="myDiv">
<form id="formID">
        <div class="mb-3">
            <!--<label for="subject" class="center-block" required>Subject</label> -->
            <input type="text" class="center-block" id="subject" placeholder="Subject of Email">
            
        </div>
        
        <div class="mb-3">
           <!-- <label for="body" class="form-label" required>Email Body</label><br> -->
            <textarea id="body" name="body" rows="4" cols="50" class="center-block" placeholder="Body of Email"></textarea>
        </div>

        <!-- Button trigger modal -->
        <!--<button type="button" class="btn btn-primary"  onclick="myFunction()" id="save">
                Submit
                </button> -->

        <button type="button"  class="center-block" id="save">Compose</button> 
  
</form>
</div>

<script>
    $(document).ready(function(){
        $('#save').on('click' , function(){
            $('#myModal').modal('show');  
            document.getElementById("hSubject").value = document.getElementById("subject").value;
            document.getElementById("hBody").value = document.getElementById("body").value;
        });
    });
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.map"></script>

</body>
</html>