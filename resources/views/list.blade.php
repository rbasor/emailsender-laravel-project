<!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>
<body>

<h2>Inbox</h2>

<a href="/">+Compose</a> 
<a href="logout">Logout</a> 
<table>
  <tr>
  <th scope="col">Sent_at </th>
      <th scope="col">To</th>
      <th scope="col">Subject</th>
      <th scope="col">Body</th>
  </tr>
  @foreach($emails as $data)
    <tr>
      <th scope="row">{{$data['created_at']}}</th>
      <td>{{$data['email']}}</td>
      <td>{{$data['subject']}}</td>
      <td>{{$data['body']}}</td>
    </tr>
    @endforeach
  
</table>

<div>
    {{$emails->links()}}
</div>

<style>
    .w-5{
        display:none
    }
</style>

</body>
</html>
