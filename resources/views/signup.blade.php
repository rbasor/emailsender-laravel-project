<h1>Sign Up Form</h1>

<form method="POST" action="signup">
    @csrf
    <div class="mb-3">
        <label for="mobile" class="form-label">Mobile Number</label>
        <input type="text" class="form-control" id="mobile" aria-describedby="emailHelp" name="mobile">
    </div>
    <div class="mb-3">
        <label for="pass" class="form-label">Password</label>
        <input type="text" class="form-control" id="pass" name="pass">
    </div>
    <div class="mb-3">
        <label for="cpass" class="form-label">Re-Enter Password</label>
        <input type="text" class="form-control" id="cpass">
    </div>
    
    <button type="submit" class="btn btn-primary">Submit</button>
</form>