<h1>Login Page</h1>

<form method="POST" action="login">
    @csrf
    <div class="mb-3">
        <label for="mobile" class="form-label">Mobile Number</label>
        <input type="text" class="form-control" id="mobile" aria-describedby="emailHelp" name="mobile">
    </div>
    <div class="mb-3">
        <label for="pass" class="form-label">Password</label>
        <input type="text" class="form-control" id="pass" name="pass">
    </div>
    
    
    <button type="submit" class="btn btn-primary">Submit</button>
</form>