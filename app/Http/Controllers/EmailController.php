<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Email;



class EmailController extends Controller
{
    //
    public function index(Request $req){
        $data= $req;
        $str_arr = explode (",", $data->email);
       
       $output = Email::all();

       foreach($str_arr as $val) {
            $user = new Email;
            $user->email = $val;
            $user->body = $data->hBody;
            $user->subject = $data->hSubject;
            $user->save();
        }

        return redirect('list');
       //return $output;
        
    }

    public function store(Request $req){
        
            $data = $req->input('mobile');
            $req->session()->put('mobile',$data);
            return view('welcome');
        
        
    }

    public function show(){
        $data = Email::orderBy('created_at', 'desc')->paginate(10);
        
        
        if(!session()->has('mobile')){
            return view('loginTable');
        }else{
            return view('list',['emails'=>$data]);
        }
    
    }
}
