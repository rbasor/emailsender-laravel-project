<?php

use Illuminate\Support\Facades\Route;

use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\EmailController;
use App\Http\Controllers\LoginController;
use App\Mail\SendMail;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(session()->has('mobile')){
        return view('welcome');
    }else
       return view('loginTable');
      //return "You logged out";
   
});
Route::get('list',[EmailController::class,'show']);

Route::post('upload',[EmailController::class,'index']);
Route::post('login',[EmailController::class,'store']);
Route::view('login','loginTable');


Route::get('logout', function () {
    
    session()->pull('mobile',null);

return view('loginTable');
});

Route::get('login', function () {
    
    
    if(!session()->has('mobile')){
        return view('loginTable');
    }else{
        return view('welcome');
    }


});

Route::get('/send-mail', function () {
    
    
    $details =[
        'title' => 'Mail from Rajbasor',
        'body' => 'This is a email body'
    ];
    \Mail::to('rajbasor1998@gmail.com')->send(new \App\Mail\SendMail($details));
    echo "Email sent!";
});




/*Route::post('signup',[LoginController::class,'index']);
Route::view('signup','signup');

Route::post('login',[LoginController::class,'find']);


Route::view('loginTable','loginTable');
*/


